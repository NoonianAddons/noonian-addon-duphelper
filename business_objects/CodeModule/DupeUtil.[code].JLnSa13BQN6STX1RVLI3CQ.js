function (db, Q, _) {
    const exports = {};
    const moment = require('moment');
    
    
    const getDupeSpec = function(className, specKey) {
        const specQuery = {};
        
        //Find relevant DupeSpec
        if(specKey) {
            specQuery.key = specKey;
        }
        else {
            specQuery['classes._id'] = db[className]._bo_meta_data.bod_id;
        }
        
        return db.DupeSpec.findOne(specQuery);
    }
    
    
    const findExistingDupList = function(forObj, compareResults, filter) {
        const queryObj = {$and:[filter]};
        const idList = _.pluck(compareResults, 'id').concat([forObj._id]);
        
        
        idList.forEach(id=>{
            queryObj.$and.push({'objects._id':id});
        });
        
        return db.DupeList.findOne(queryObj).then(dl=>{
            
            if(dl && dl.objects && dl.objects.length === idList.length) {
                return dl;
            }
            return null;
        });
    }
    
    //create DupeList from DupeSpec.identifyDuplicates result
    const createDupList = function(dupeSpec, forObj, compareResults, scanLabel) {
        const className = forObj._bo_meta_data.class_name;
        if(compareResults && compareResults.length) {
            return findExistingDupList(forObj, compareResults, {status:'outstanding'}).then(existing=>{
            
                if(existing) {
                    return;
                }
                
                const refList = [{_id:forObj._id, ref_class:className}];
                
                _.forEach(compareResults, r=>{
                    refList.push({_id:r.id, ref_class:className});
                });
                
                const dupeList = new db.DupeList({
                    status:'outstanding',
                    spec:{_id:dupeSpec._id},
                    extended_info:{compareResults},
                    objects:refList, 
                    scan:scanLabel
                });
                return dupeList.save();
                
            });    
        }
        
        return null;
    };
    
    
    const MatchTable = function(dupeLists) {
        
        _.forEach(dupeLists, dl=>{
            var idList = _.pluck(dl.objects, '_id');
            _.forEach(idList, id=>{
                var appendTo = this[id] || [];
                this[id] = _.unique(appendTo.concat(idList));
            })
        })
    };
    
    MatchTable.prototype.isMatched = function(a, b) {
        var found = false;
        _.forEach(this[a], id=>{
            found = found || (id===b);
        });
        
        return found;
    };
    
    const getFlaggedFalseMatches = function(dupeSpec) {
        return db.DupeList.find({spec:dupeSpec._id, status:'false_match'}).then(dlList=>{
            if(dlList && dlList.length) {
                return new MatchTable(dlList);
            }
            else {
                return {isMatched:()=>false};
            }
        })
        
        
    }
    
    
    /**
     * Find potential duplicates for a particular object; optionally save the dupList
     */
    exports.findDuplicates = function(forObj, saveDupList, specKey) {
        
        if(!forObj) {
            return Q.reject('Missing required param');
        }
        
        return getDupeSpec(forObj._bo_meta_data.class_name, specKey).then(dupeSpec=>{
            if(!dupeSpec) {
                throw new Error('missing DupeSpec for '+(specKey || forObj._bo_meta_data.class_name));
            }
            
            return getFlaggedFalseMatches(dupeSpec).then(matchTable => {
                
                var resultPromise = dupeSpec.identifyDuplicates(forObj, matchTable);
                
                if(saveDupList) {
                    resultPromise = resultPromise.then(createDupList.bind(null, dupeSpec, forObj));
                }
                
                return resultPromise;
            });
        });
    };
    
    
    
    
    /**
     * Do a full scan on specified class; generate a set of DupeList objects to mark groups of duplicates
     *  
     */
    exports.scan = function(className, specKey, scanLabel, filterQuery) {
        
        return getDupeSpec(className, specKey).then(dupeSpec=>{
            if(!dupeSpec) {
                throw new Error('missing DupeSpec for '+(specKey || className));
            }
            
            return Q.all([dupeSpec, getFlaggedFalseMatches(dupeSpec)]);
            
        }).spread((dupeSpec, matchTable)=>{
            
        
            scanLabel = scanLabel || moment().format('YYYY-MM-DD HH:mm:ss');
            
            const processedItems = {};
            
            const processItem = function(obj) {
                if(processedItems[obj._id]) {
                    return;
                }
                processedItems[obj._id] = true;
                return dupeSpec.identifyDuplicates(obj, matchTable).then(compareResults=>{
                    
                    if(compareResults && compareResults.length) {
                        _.forEach(compareResults, r=>{
                            processedItems[r.id] = true;
                        });
                        
                        return createDupList(dupeSpec, obj, compareResults, scanLabel);
                    }
                })
            };
            
            filterQuery = filterQuery || {}; 
            
            return db[className].find(filterQuery).then(testList=>{
                
                var promiseChain = Q(true);
                
                _.forEach(testList, item=>{
                    promiseChain = promiseChain.then(processItem.bind(null, item));
                });
                
                return promiseChain.then(()=>{
                    return {scan:scanLabel};
                });
                
            });
        });
    };
    
    
    
    
    return exports;
}