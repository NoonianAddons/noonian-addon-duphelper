function(db, _, Q) {
    
    const stringify = require('json-stable-stringify');
    
    const IDENTICAL='identical',ONE_EMPTY='one_empty',COMBINED='combined';
    
    const exports = {
        IDENTICAL,
        ONE_EMPTY,
        COMBINED
    };
    
    
    const simpleCompare = function(a, b) {
        return a===b;
    };
    
    const objCompare = function(a, b) {
        return stringify(a) === stringify(b);
    };


    const arrayCompare = function(td, a, b) {
        
        if(!a && !b) {
            return true;
        }
        if(!a || !b || a.length !== b.length) {
            return false;
        }
        
        const compareFn = exports.singleValueCompareFunctions[td.type] || objCompare;
        var cmpA = a, cmpB = b;
        
        if(!td.orderMatters) {
            cmpA = _.sortBy(a, stringify);
            cmpB = _.sortBy(b, stringify);
        }
        
        var equals = true;
        for(let i=0; i < cmpA.length; i++) {
            equals = equals && compareFn(cmpA[i], cmpB[i]);
        }
        return equals;
    };
        
    
    const singleValueCompareFunctions =
    exports.singleValueCompareFunctions = {
        string:simpleCompare,
        text:simpleCompare,
        path:simpleCompare,
        date:simpleCompare,
        object:objCompare,
        reference:function(a, b) {
            return (a && a._id) == (b && b._id);
        }
    };
    
    const arrayValueCompareFunctions = 
    exports.arrayValueCompareFunctions = {
        reference:function(a, b) {
            a = _.compact(_.pluck(a, '_id'));
            b = _.compact(_.pluck(b, '_id'));
            return arrayCompare({type:'string'}, a, b);
            
        }
    };
    
    const getCompareFunction = 
    exports.getCompareFunction = function(td) {
        const isArray = td instanceof Array;
        td = isArray ? td[0] : td;
        
        if(isArray) {
            return arrayValueCompareFunctions[td.type] || arrayCompare.bind(null, td);
        }
        return singleValueCompareFunctions[td.type] || objCompare;
    };
    
    
    const textMerge = function(td, a, b) {
        let separator = td.mergeSeparator || '\n';
        if(a == b) {
            return {
                status:IDENTICAL,
                value:a
            };
        }
        else if(a && b) {
            let ret = {status:COMBINED};
            if(a.indexOf(b) > -1) {
                ret.value = a;
            }
            else if(b.indexOf(a) > -1) {
                ret.value = b;
            }
            else {
                ret.value = a+separator+b;
            }
            return ret;
        }
        else {
            return {
                status:ONE_EMPTY,
                value:(a || b)
            };
        }
    };
    
    const mergeArray = function(td, a, b) {
        //Consider null/undefined == empty array (since mongoose automatically populates array fields as such)
        a = a || [];
        b = b || [];
        
        if(!a.length && !b.length) {
            return {status:IDENTICAL, value:a};
        }
        else if(!a.length || !b.length) {
            return {
                status:ONE_EMPTY,
                value: a.length ? a : b
            };
        }
        else if(arrayCompare(td, a, b)) {
            return {
                status:IDENTICAL,
                value:a
            }
        }
        else {
            //Two non-empty, non-equal arrays
            let merged = _.clone(a);
            let aStr = a.map(stringify), bStr = b.map(stringify);
            
            var anyChange = false;
            
            //add each of b if not already in a
            for(var i = 0; i < b.length; i++) {
                if(aStr.indexOf(bStr[i]) < 0) {
                    merged.push(b[i]);
                }
            }
            
            return {
                status:COMBINED,
                value:merged
            }
        }
    };
    
    const mergeRefArray = function(td, a, b) {
        a = _.sortBy(_.compact(a), '_id');
        b = _.sortBy(_.compact(b), '_id');
        
        if(!a.length && !b.length) {
            return {status:IDENTICAL, value:[]};
        }
        else if(!a.length || !b.length) {
            return {
                status:ONE_EMPTY,
                value: a.length ? a : b
            };
        }
        else {
            //same ids?
            if(_.isEqual(_.pluck(a, '_id'), _.pluck(b, '_id'))) {
                return {status:IDENTICAL, value:a};
            }
            let refMap = _.indexBy(a.concat(b), '_id');
            let allIds = Object.keys(refMap);
            let merged = allIds.map(_id=>{
                return {_id, _disp:refMap[_id]._disp};
            });
            
            return {
                status:COMBINED,
                value:merged
            };
            
        }
    };
    
    
    //Just check if one is null and the other isnt
    const defaultSingleValueMerge = function(td, a, b) {
        let aNull = (a == null), bNull = (b == null);
        
        if(a === b || (aNull && bNull)) {
            return {
                status:IDENTICAL,
                value:a
            };
        }
        if(aNull || bNull) {
            return {
                status:ONE_EMPTY,
                value:(aNull ? b : a)
            };
        }
        
        return undefined;
    };
    
    const singleValueMergeFunctions =
    exports.singleValueMergeFunctions = {
        text:textMerge
    };
    
    const arrayValueMergeFunctions = 
    exports.arrayValueMergeFunctions = {
        reference:mergeRefArray
    };
    
    
    /*
     * auto-merge function general contract:
     *  parameters: (typeDescriptor, valueA, valueB)
     *  return value: {status:<COMBINED|IDENTICAL|ONE_EMPTY>, value:<merged value>}
    */
    const getAutoMergeFunction =
    exports.getAutoMergeFunction = function(td) {
        const isArray = td instanceof Array;
        td = isArray ? td[0] : td;
        
        if(isArray) {
            return arrayValueMergeFunctions[td.type] || mergeArray;
        }
        return singleValueMergeFunctions[td.type] || defaultSingleValueMerge;
    };
    
    
    
    const getCombinedMergeStatus = function(x, y) {
        if(x === y) {
            return x;
        }
        if(!x || !y) {
            return x || y;
        }
        if(x === COMBINED || y === COMBINED) {
            return COMBINED;
        }
        //both not equal, not null, and not "combined" ->
        // identical and one_empty
        return ONE_EMPTY;
    };
    
    /**
     * attempt to merge all values in an array
     * @return null if auto-merge not possible
    */
    const attemptAutoMerge =
    exports.attemptAutoMerge = function(td, valueArr) {
        
        let mergeFn = getAutoMergeFunction(td);
        
        if(!valueArr || !valueArr.length || !mergeFn) {
            return null;
        }
        else if(valueArr.length === 1) {
            return {status:IDENTICAL, value:valueArr[0]};
        }
        
        var a, b;
        var status;
        
        for(let i=0; i < valueArr.length-1; i++) {
            
            a = a || valueArr[i];
            
            for(let j=i+1; j < valueArr.length; j++) {
                b = valueArr[j];
                let mergeResult = mergeFn(td, a, b);
                
                if(mergeResult) {
                    a = mergeResult.value;
                    status = getCombinedMergeStatus(status, mergeResult.status);
                }
            }
        }
        
        return status && {status, value:a};
        
    };
    
    
    const nullOrUndefined = val=>(val == null);
    const emptyArray = val=>(val instanceof Array && !val.length);
    
    /**
     * @return object {fieldName:{safe:<boolean>, source:'id'} }
     */
    exports.computeDiffs = function(objList, autoMerge) {
        
        if(!objList || !objList.length) {
            throw 'Missing objects for comparison';
        }
        
        const result = {};
        
        const typeDescMap = objList[0]._bo_meta_data.type_desc_map;
        
        _.forEach(typeDescMap, (td, f)=>{
            
            if(f.indexOf('_') === 0) {
                return;
            }
            
            const compareFn = getCompareFunction(td);
            
            const nullCheckFn = td instanceof Array ? emptyArray : nullOrUndefined;
            
            //Identify non-null values (empty array counts as null)
            const nonNulls = {}; //_id -> value
            _.forEach(objList, obj=>{
                let val = obj[f];
                if(!nullCheckFn(val)) {
                    nonNulls[obj._id] = val;
                }
            });
            
            const nonNullIds = Object.keys(nonNulls);
            
            if(nonNullIds.length === 0) {
                //All null! Don't worry about merge.
                return; //next field
            }
            if(nonNullIds.length === 1) {
                //One single value w/ non-null; that's the one to keep!
                let keeperId = nonNullIds[0];
                result[f] = {
                    safe:true,
                    source:keeperId,
                    value:nonNulls[keeperId]
                };
                return; //next field
            }
            
            //all non-nulls equal?
            var allEqual = true;
            nonNullIds.forEach(id1=>{
                nonNullIds.forEach(id2=>{
                    if(id1 !== id2) {
                        allEqual = allEqual && compareFn(nonNulls[id1], nonNulls[id2]);
                    }
                });
            });
            
            if(allEqual) {
                let keeperId = nonNullIds[0];
                result[f] = {
                    safe:true,
                    source:keeperId,
                    value:nonNulls[keeperId]
                };
                return; //next field
            }
            
            
            //There are multiple different non-null values 
            // Group the objects that have the same value
            
            var a, b, aGrp, bGrp; //vars for iteration
            const groupings = {}; //_id -> [ids w/ same value for f]
            
            for(let i=0; i < objList.length-1; i++) {
                a = objList[i];
                aGrp = groupings[a._id] = groupings[a._id] || [a._id];
                
                for(let j=i+1; j < objList.length; j++) {
                    b = objList[j];
                    bGrp = groupings[b._id] = groupings[b._id] || [b._id];
                    
                    if(compareFn(a[f], b[f])) {
                        //merge the groupings!
                        let merged = _.union(aGrp, bGrp);
                        aGrp = groupings[a._id] = groupings[b._id] = merged;
                    }
                    
                }
            }
            
            //Extract the distinct groups from groupings
            
            const groupList = []; //distinct groups (arrays if ids)
            const added = {}; 
            
            _.forEach(groupings, (grp, id)=>{
                if(!added[id]) {
                    groupList.push(grp);
                    grp.forEach(i=>{added[i]=true});
                }
            });
            
            if(groupList.length === 1) {
                //All equal!  nothing to report
                return;
            }
            else {
                //we have different possible values for f
                //TODO associate each grouping w/ corresponding value; attempt auto-merge for each
                result[f] = {
                    safe:false,
                    groups:groupList
                };
                
                if(autoMerge && getAutoMergeFunction(td)) {
                    let valueArr = [];
                    _.forEach(groupList, grp=>{
                        valueArr.push(
                            nonNulls[grp[0]]
                        );
                    });
                    result[f].autoMerge = attemptAutoMerge(td, valueArr);
                }
            }
            
            
            
        }); //end typeDescMap forEach iteration
        
        return result;
        
    };

    
    
    const saveObj = function() {
        return this.save({skipTriggers:true});
    };
    
    
    //Similar to _.get(obj, path), except it digs into arrays, and returns an array of all values under fieldPath
    const resolveValue = function(contextObj, fieldPath) {
      const dotPos = fieldPath.indexOf('.');
      if(dotPos < 0) {
        let val = contextObj[fieldPath];
    
        return val instanceof Array ? val : [val];
      }
    
      const baseField = fieldPath.substring(0, dotPos);
      const subPath = fieldPath.substring(dotPos+1);
    
      var valArray = contextObj[baseField];
    
      valArray = valArray instanceof Array ? valArray : [valArray];
    
      const ret = [];
      valArray.forEach(v=>{
        resolveValue(v, subPath).forEach(resolved=>{
          ret.push(resolved);
        })
      });
      return ret;
    
    };

    
    /**
     * For reference field fromClass.fromField pointing to toClass.oldId, 
     *   redirect it to toClass.newId
     */
    const updateReferencingObjects = function(fromClass, fieldPath, toClass, oldId, newId) {
        
        const targetDisp = this.newTarget._disp;
        
        const refQuery = {};
        refQuery[fieldPath+'._id'] = oldId;
        
        return db[fromClass].find(refQuery).then(referencingObjects => {
           
           var promiseChain = Q(true);
           _.forEach(referencingObjects, refObj=>{
               console.log('DUP FIXER: Updating reference from %s.%s[%s] to %s.%s', 
                    fromClass, refObj._id, fieldPath, toClass, newId
               );
               
               resolveValue(refObj, fieldPath).forEach(r=>{
                //   console.log('%s -> %j', fieldPath, r);
                    if(r && r._id === oldId) {
                        let originalRef = _.clone(r);
                        
                        r._id = newId;
                        r._disp = targetDisp;
                        
                        this.redirects.push({
                            className:fromClass,
                            id:refObj._id,
                            field:fieldPath,
                            originalRef
                        });
                    }
               });
               
               let dotPos = fieldPath.indexOf('.');
               let baseField = dotPos > 0 ? fieldPath.substring(0, dotPos) : fieldPath;
               
               refObj.markModified(baseField);
               
               promiseChain = promiseChain.then(saveObj.bind(refObj));
           });
           
           
           return promiseChain;
       });
    }
    
    
    /**
     * identify all references in fromClass to toClass;
     *   (recursively search all composites)
     * any references to toClass.fromId are updated to toClass.toId
     */
    const redirectRefsForClass = function(fromClass, toClass, fromId, toId, tdMap, fieldPrefix) {
        
        if(!db[fromClass]) {
            throw 'invalid reference class: '+fromClass;
            
        }
        
        tdMap = tdMap || db[fromClass]._bo_meta_data.type_desc_map;
        fieldPrefix = fieldPrefix || '';
        
        var promiseChain = Q(true);
        _.forEach(tdMap, (td, fieldName) => {
           let isArray = td instanceof Array;
           td = isArray ? td[0] : td;
            
           if(td.type === 'reference' && td.ref_class === toClass) {
               //Reference or array of references to toClass -> call the update logic for this path
                promiseChain = promiseChain.then(
                    updateReferencingObjects.bind(this, fromClass, fieldPrefix+fieldName, toClass, fromId, toId)
                );
           }
           else if(td.type === 'composite' || td.is_composite) {
               //Composite or array of composites -> recursively check composite typedesc, building field path along the way
               if(td.type_desc_map) {
                   promiseChain = promiseChain.then(
                       redirectRefsForClass.bind(this, fromClass, toClass, fromId, toId, td.type_desc_map, fieldPrefix+fieldName+'.')
                   )
               }
           }
       });
       
       return promiseChain;
    };
    
    
    /**
     * Find all references in the database pointing to toClass.fromId and point them to toClass.toId
     */
    const redirectIncomingReferences = 
    exports.redirectIncomingReferences = function(toClass, fromId, toId) {
        
        const THIS = {
            redirects:[],
            targetClass:toClass,
            targetId:toId
        };
        
        return Q.all([
            db.BusinessObjectDef.find({}, {_id:1, class_name:1}),
            db[toClass].findOne({_id:toId})
        ])
        .spread((bodList, newTarget)=>{
            
            THIS.newTarget = newTarget;
            
            var promiseChain = Q(true);
            _.forEach(bodList, bod => {
                if(db[bod.class_name]) {
                    promiseChain = promiseChain.then(
                        redirectRefsForClass.bind(THIS, bod.class_name, toClass, fromId, toId, null, null)
                    );
                }
            });
            return promiseChain;
        })
        .then(()=>{
            return THIS.redirects;
        })
        ;
    };


    const restoreRef = function(obj, field, fromRefId, toRef) {
        console.log('Revert %s.%s %s -> %s', obj._id, field, fromRefId, toRef);
        let dotPos = field.indexOf('.');
        let baseField = dotPos > 0 ? field.substring(0, dotPos) : field;
        
        resolveValue(obj, field).forEach(r=>{
            if(r && r._id === fromRefId) {
                _.assign(r, toRef);
            }
       });
       
       obj.markModified(baseField);
       
    };
    
    const restoreRefsForObj = function(fromRefId, refDescList) {
        console.log(fromRefId, refDescList);
        if(refDescList.length) {
            let className = refDescList[0].className;
            let id = refDescList[0].id;
            
            return db[className].findOne({_id:id}).then(obj=>{
                if(!obj) {
                    return;
                }
                _.forEach(refDescList, rd=>{
                    restoreRef(obj, rd.field, fromRefId, rd.originalRef);
                })
                return obj.save();
            });
            
        }
    };
    
    exports.restoreReferences = function(fromRefId, redirectList) {
        
        var promiseChain = Q(true);
        
        var indexedRedirects = _.groupBy(redirectList, 'id');
        _.forEach(indexedRedirects, (refDescList, id)=>{
            promiseChain = promiseChain.then(restoreRefsForObj.bind(null, fromRefId, refDescList));
        });
        
        return promiseChain;
    };
    
    return exports;
}