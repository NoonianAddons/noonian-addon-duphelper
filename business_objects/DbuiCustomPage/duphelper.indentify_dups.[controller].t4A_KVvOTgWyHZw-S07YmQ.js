function (db, $scope, $state,  $stateParams, $q, $timeout, NoonWebService, DbuiFieldType, DbuiAlert) {
    
    var bgColors = ['#004586','#ffd320','#ff420e','#579d1c','#7e0021','#83caff','#314004'];
    var fgColors = ['white','black','black','black','white','black','white'];
    var letters = 'ABCDEFGHIJKLMNOP';
    
    var className = $scope.className = $stateParams.className;
    var perspectiveName = $stateParams.perspective || 'default';
    
    if(!className || !db[className]) {
        $scope.errorMessage = 'Loaded without proper parameters';
        return;
    }
    
    var TargetModel = db[className];
    var tdMap = $scope.typeDescMap = TargetModel._bo_meta_data.type_desc_map;
    var fieldList;
    
    
    const tabStatus = $scope.tabStatus = {activeTab:'A'};
    const compTabs = $scope.compTabs = [
        {index:'A',bgColor:bgColors[0],fgColor:fgColors[0]},
        {index:'B',bgColor:bgColors[1],fgColor:fgColors[1]}
    ];
    var tabIndex = _.indexBy(compTabs, 'index');
    var tabsByObjectId;
    
    $scope.newTab = function() {
        var idx = compTabs.length;
        var newTab = {
            index:letters[idx],
            bgColor:bgColors[idx],
            fgColor:fgColors[idx]
        };
        compTabs.push(newTab);
        tabIndex[newTab.index] = newTab;
        $timeout(function(){
            tabStatus.activeTab = newTab.index;
        }, 100);
        
    };
    
    const updateFilter = function() {
        var selected = [];
        _.forEach(compTabs, t => {
            if(t.id) {
                selected.push({_id:{$ne:t.id}});
            }
        });
        
        var query = null;
        if(!selected.length) {
            query = {};
        }
        else if(selected.length == 1) {
            query = selected[0];
        }
        else {
            query = {$and:selected};
        }
        // console.log('setting query', query);
        
        $scope.listPerspective.filter = query;
        $scope.refreshForms();
    };
    
    const checkMergeReady = function() {
        var selectedCount = 0;
        _.forEach(compTabs, t => {
            t.object && selectedCount++;
        
        });
        
        $scope.mergeReady = (selectedCount > 1);
        $scope.selectedCount = selectedCount;
    }
    
    const selectListItem = function(selection) {
        var selectedObj = selection.targetObj;
        var myTab = tabIndex[tabStatus.activeTab];
        myTab.id = selectedObj._id;
        myTab.object = TargetModel.findOne({_id:selectedObj._id});
        updateFilter();
        myTab.object.$promise.then(checkMergeReady);
    };
    
    $scope.unselect = function(t) {
        t.object = null;
        t.id = null;
        updateFilter();
        checkMergeReady();
    }
    
    $q.all([
        Dbui.getPerspective(perspectiveName, className, 'diff-compare'),
        DbuiFieldType.cacheTypeInfoForClass(className, 'edit')
    ])
    .then(function([perspective]) {
        // console.log(perspective);
        $scope.editPerspective = perspective;
        $scope.listPerspective = perspective;
        
        perspective.recordActions = ['dialog-view', {icon:'fa-check-square', fn:selectListItem}];
        
        fieldList = [];
        _.forEach(perspective.layout, function(sec) {
            _.forEach(sec.rows, function(row) {
                _.forEach(row, function(f) {
                    fieldList.push(f);
                });
            });
        });
        
    },
    function(err) {
        console.error('problem retreiving perspectives', err);
    }
    )
    ;
    
    
    
    
    $scope.doMerge = function() {
        console.log(compTabs);
        const idList = [];
        _.forEach(compTabs, t => {
            t.object && idList.push(t.object._id);
        });
        
        NoonWebService.call('duphelper/newDupeList', {className, ids:idList}).then(result=>{
            console.log(result);
            if(result.simple) {
                $scope.mergeComplete = result;
            }
            else {
                if(result && result.id) {
                    $state.go('dbui.merge_dups', result);
                }
            }
        },
        err=>{
            console.error(err);
        });
    };
    
    
    $scope.refreshForms = function() {
         $scope.refreshing = true;
         $timeout(function() {
             $scope.refreshing = false;
            //  wiredup = false;
         }, 100);
    };
     
}