function (db, $scope, $stateParams, $q, $timeout, NoonWebService, DbuiFieldType, DbuiAlert) {
    
    const bgColors = ['#004586','#ffd320','#ff420e','#579d1c','#7e0021','#83caff','#314004'];
    const fgColors = ['white','black','black','black','white','black','white'];
    const recordLabels = 'ABCDEFG';
    
    
    const dupeListId = $stateParams.id;
    
    const perspectiveName = $stateParams.perspective || 'default';
    console.log('merge_dups with perspective:', perspectiveName);
    
    var className, dupeList, TargetModel, tdMap, compTabs;
    
    const tabIndex = {};
    const tabsByObjectId = {};
    const tabStatus = $scope.tabStatus = {activeTab:'A'};
    
    
    $scope.keeper = 0;
    
    NoonWebService.call('duphelper/getMergeData', {dupelist:dupeListId}).then(function(mergeData){
        
        if(!mergeData) {
            $scope.errorMessage = 'Invalid dupelist'
            return;
        }
        console.log('mergeData', mergeData);
        className = $scope.className = mergeData.className;
        TargetModel = db[className];
        
        $scope.labelGroup = TargetModel._bo_meta_data.field_labels;
        tdMap = $scope.typeDescMap = TargetModel._bo_meta_data.type_desc_map;
        
        dupeList = $scope.dupeList = mergeData.dupeList;
        
        compTabs = $scope.compTabs = [];
        let i=0;
        
        _.forEach(mergeData.objects, function(o){
            let obj = new db[className](o);
            let t = {
                object:obj, 
                index:recordLabels.charAt(i),
                bgColor:bgColors[i],
                fgColor:fgColors[i],
                keeper:(i===0)
            };
            
            tabIndex[t.index] = t;
            tabsByObjectId[obj._id] = t;
            
            compTabs.push(t);
            i++;
        });
        
        
        var mergeStatus = $scope.mergeStatus = {};
        var mergedObj = $scope.mergedObj = new db[className](mergeData.objects[0]);
        
        _.forEach(mergeData.diffInfo, function(ifo, f){
            
            if(ifo.safe) {
                let tab = tabsByObjectId[ifo.source];
                mergeStatus[f] = {
                    status:'one_empty',
                    source:tab.index
                };
                mergedObj[f] = tab.object[f];
            }
            else if(ifo.autoMerge) {
                mergeStatus[f] = {
                    status:ifo.autoMerge.status
                };
                mergedObj[f] = ifo.autoMerge.value;
            }
            else {
                
                var selectGroups = [];
                var defaultSource = null;
                _.forEach(ifo.groups, function(g) {
                    let tab = tabsByObjectId[g[0]];
                    if(g[0] === mergedObj._id) {
                        defaultSource = tab;
                    }
                    
                    if(tab.object[f] != null) {
                        let labelParts = [];
                        _.forEach(g, function(objId){
                            labelParts.push(tabsByObjectId[objId].index);
                        });
                        labelParts.sort();
                        selectGroups.push({
                            tab,
                            label:labelParts.join(',')
                        });
                    }
                });
                if(!defaultSource) {
                    defaultSource = selectGroups[0].tab;
                    mergedObj[f] = defaultSource.object[f];
                }
                mergeStatus[f] = {
                    status:'manual_merge_required',
                    source:defaultSource.index,
                    sourceTab:defaultSource,
                    selectGroups
                };
                // console.log(f, mergeStatus[f]);
            }
        });
        
        _.forEach(tdMap, function(td, f) {
            if(!mergeStatus[f]) {
                mergeStatus[f] = {status:'identical'};
            }
        });
        
        
        $q.all([
            Dbui.getPerspective(perspectiveName, className, 'view'),
            Dbui.getPerspective(perspectiveName, className, 'edit'),
            DbuiFieldType.cacheTypeInfoForClass(className, 'edit')
        ])
        .then(function([viewPerspective, editPerspective]) {
            console.log('viewPerspective', viewPerspective);
            var perspective = $scope.editPerspective = editPerspective;
            $scope.viewPerspective = viewPerspective;
            
            let fieldList = $scope.fieldList = [];
            _.forEach(perspective.layout, function(sec) {
                _.forEach(sec.rows, function(row) {
                    _.forEach(row, function(f) {
                        fieldList.push(f);
                    });
                });
            });
            
        });
        
        
    },
    function(err) {
        $scope.errorMessage = err;
    });
    
    
    
    $scope.markKeeper = function(t) {
        var i = 0;
        _.forEach(compTabs, ct=>{
            ct.keeper = false;
            if(t === ct) {
                $scope.keeper = i;
                $scope.mergedObj._id = t.object._id;
            }
            i++;
        });
        t.keeper = true;
    }
    
    //Stuff for merge pane:
    $scope.colClass = Dbui.columnClasses;
    $scope.linkStatus = {};
    
    // $scope.$watch('linkStatus', function(ls) {
    //     $scope.editorForm.$setPristine();
    //     if($scope.formStatus) $scope.formStatus.isDirty = false;
    // }, true);
    
    
    
    
    $scope.commitMerge = function(fullDelete) {
        
        var keeper = $scope.keeper;
        var keeperId = compTabs[keeper].object._id;
        var deleting = _.pluck(compTabs, 'index');
        deleting.splice(keeper, 1);
        
        if(fullDelete && !confirm('Are you sure you want to delete records: '+deleting.join(','))) {
            return;
        }
        
        
        return NoonWebService.post('duphelper/commitMerge', {
          dupeList: dupeListId,
          deleteDuplicates:fullDelete,
          keeper:keeperId
        },
        {mergedObject: $scope.mergedObj}
        )
        .then(function(wsResult) {
            
            $scope.resultMessage = wsResult.message;
            $scope.mergeComplete = true;
            DbuiAlert.success(wsResult.message);
            
        },
        function(err) {
            console.log('error saving merged', err);
            DbuiAlert.danger(err);
        });
        
    };

    
    
    
    var statusToClass = {
        manual_merge_required:'bg-danger',
        one_empty:'bg-success',
        identical:'bg-success',
        combined:'bg-warning'
    };
    $scope.getMergeStatusClass = function(f) {
        return statusToClass[$scope.mergeStatus[f].status];
    };
    
    $scope.getMergePickerStyle = function(f) {
        var t = $scope.mergeStatus[f].sourceTab;
        return `background-color:${t.bgColor};color:${t.fgColor}`;
    };
    
    $scope.getTextForStatus = function(f) {
        var s = $scope.mergeStatus[f];
        if(s.status === 'one_empty')  {
            return `Using value from ${s.source} (other value empty)`;
        }
        if(s.status === 'identical') {
            return 'Identical values';
        }
        if(s.status === 'manual_merge_required') {
            return 'Different values in each record.  Merge Manually';
        }
        if(s.status === 'combined') {
            return 'Automatically combined the values';
        }
        
    };
    
    
    $scope.getDisplayValue = function(t, fieldName) {
        var val = t.object[fieldName];
        
        return val._disp || val;
    };
    
    
    const refreshField = $scope.refreshField = {};
    $scope.pickValue = function(t, fieldName) {
        console.log('pickValue', t, fieldName)
        $scope.mergedObj[fieldName] = t.object[fieldName]
        var ms = $scope.mergeStatus[fieldName];
        ms.sourceTab = t;
        ms.source = t.index;
        // console.log(`mergeStatus for ${fieldName}`, ms);
        
        refreshField[fieldName] = true;
        $timeout(function() {
            refreshField[fieldName] = false;
        }, 100);
        
    };
    
    
    $scope.refreshForms = function() {
         $scope.refreshing = true;
         $timeout(function() {
             $scope.refreshing = false;
            //  wiredup = false;
         }, 100);
    };
    
    const unsafeStatus = {
        combined:true,
        manual_merge_required:true
    }
    var hideSafe = $scope.hideSafe = false;
    $scope.showField = function(f) {
        return !hideSafe || unsafeStatus[$scope.mergeStatus[f].status];
    };
    $scope.toggleHideSafe = function() {
        hideSafe = $scope.hideSafe = !hideSafe;
    }
     
}