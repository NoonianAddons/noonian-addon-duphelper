function(db, _, Q, MergeUtil, logger, nodeRequire) {
    
    const invoker = nodeRequire('../util/invoker');
    const VersionId = nodeRequire('../api/datasource/version_id');
    
    return function(mergedVersion, keeperId) {

        var keeper, dups;
        var className, dupIds, verIds;
        
        
        const redirectResults = [];
        var interceptors, interceptorResults;
        
    
        return db.DupeSpec.findOne({_id:this.spec._id}).then(spec=>{
          
            if(spec.merge_interceptor) {
                interceptors = invoker.invokeInjected(spec.merge_interceptor, {}, spec);
            }
            else {
                interceptors = {};
            }
            
            var objPromise = this.getObjects();
            
            if(interceptors.preMerge instanceof Function) {
                objPromise = objPromise.then(objList=>{
                    return Q(interceptors.preMerge(this, objList, mergedVersion)).then(result=>{
                        if(result) {
                            interceptorResults = {preMerge:result};
                        }
                        return objList;
                    });
                });
            }
          
            return objPromise;
        })
        .then(objList=>{
            
            //(1) Save revert data
            
            const objMap = _.indexBy(objList, '_id');
            // logger.debug('%j\n***\n%j', this.objects, Object.keys(objMap));
            
            var keeperIndex = 0;
            if(keeperId) {
                var idList = _.pluck(this.objects, '_id');
                keeperIndex = idList.indexOf(keeperId);
            
                if(keeperIndex < 0) {
                    throw new Error('Invalid keeperId specified'+keeperId);
                }
            }
            
            keeper = objMap[this.objects[keeperIndex]._id];
            
            className = keeper._bo_meta_data.class_name
            
            dups = [];
            for(let i=0; i < this.objects.length; i++) {
                if(i!==keeperIndex) {
                    dups.push(objMap[this.objects[i]._id].toPlainObject());
                }
            }
            
            dupIds = _.pluck(dups, '_id');
            verIds = _.pluck(objList, '__ver');
            
            this.revert_data = {keeper:keeper.toPlainObject(), dups, className};
            this.status = 'in_process';
            
            return this.save();
        })
        .then(()=>{
            logger.debug('saving merged version of %s %s', className, keeper._id);
            //(2) Save merged version over the first one in the list (the "keeper")
            delete mergedVersion.__ver;
            _.assign(keeper, mergedVersion);
            
            if(this.merge_vids) {
                var vid = new VersionId(verIds[0]);
                for(let i=1; i < verIds.length; i++) {
                    vid = VersionId.merge(vid, new VersionId(verIds[i]));
                }
                return keeper.save({useVersionId:vid.toString()});
            }
            else {
                return keeper.save();
            }
        })
        .then(()=>{
            //(3) re-direct references from other items to the keeper
            var promiseChain = Q(true);
            
            const redirectRefs = function(dup) {
                return MergeUtil.redirectIncomingReferences(className, dup._id, keeper._id).then(result=>{
                    redirectResults.push(result)
                });
            };
            
            
            dups.forEach(d=>{
                promiseChain = promiseChain.then(redirectRefs.bind(this, d));
            });
            
            return promiseChain;
        })
        .then(()=>{
            //(4) Compile all redirect refs, stash into my revert_data
            this.revert_data.redirects = Array.prototype.concat.apply([], redirectResults);
            this.markModified('revert_data');
            
            this.status = 'merged';
            
            
            return Q.all([
                this.save(), 
                db[className].remove({_id:{$in:dupIds}})
            ]);
            
        })
        .then(()=>{
            
            if(interceptors.postMerge instanceof Function) {
                return Q(interceptors.postMerge(this, keeper)).then(result=>{
                    if(result) {
                        interceptorResults = interceptorResults || {};
                        interceptorResults.postMerge = result;
                    }
                });
            }
            
        })
        .then(()=>{
            if(interceptorResults) {
                this.extended_info = this.extended_info || {};
                this.extended_info.interceptorResults = interceptorResults;
                this.markModified('extended_info');
                return this.save();
            }
        })
        .then(()=>{
            return {
                keeper:keeper._id,
                deleted:dupIds,
                redirectedRefs:this.revert_data.redirects,
                interceptorResults
            };
        })
        ;
        
        
    }
}