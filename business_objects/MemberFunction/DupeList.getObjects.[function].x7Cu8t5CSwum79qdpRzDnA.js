function(db, _, Q) {
    
    return function() {
        
        var className;
        var idList = {};
        var error = null;
        
        _.forEach(this.objects, ref=>{
            if(error) return;
            if(ref && ref._id) {
                if(className && ref.ref_class !== className) {
                    error = 'Objects of different classes';
                }
                else {
                    idList[ref._id] = true;
                    className = ref.ref_class;
                }
            }
        });
        
        idList = Object.keys(idList);
        
        if(idList.length < 2) {
            error = 'Too few objects';
        }
        
        if(!db[className]) {
            error = 'Invalid reference to '+className;
        }
        
        if(error) {
            return Q.reject(error);
        }
        
        
        
        //Grab all the objects
        return db[className].find({_id:{$in:idList}}).then(objList=>{
            if(!objList || objList.length < idList.length) {
                throw 'Invalid object references';
            }
            return objList;
        });
    }
}