function(MergeUtil) {
    
    return function(autoMerge) {
        return this.getObjects().then(objList=>MergeUtil.computeDiffs(objList, autoMerge));
    }
}