function(db, _, Q, MergeUtil, logger, nodeRequire, PromiseUtil) {
    
    const invoker = nodeRequire('../util/invoker');
    const VersionId = nodeRequire('../api/datasource/version_id');
    
    
    //look at UpdateLogs for objects referenced in mergeData, 
    //  pull in dates that respective fields were updated
    const augmentMergeData = function(mergeData, coalesceWith) {
        
        var sourceIds = {};
        _.forEach(mergeData, (valueList, field)=>{
            _.map(valueList, 'source_id').forEach(id=>(sourceIds[id]=true));
        });
        sourceIds = Object.keys(sourceIds);
        
        return db.UpdateLog.find({object_id:{$in:sourceIds}}).sort({timestamp:-1}).then(updateLogs=>{
            updateLogs = _.groupBy(updateLogs, 'object_id');
            
            _.forEach(mergeData, (valueList, field)=>{
                valueList.forEach(v=>{
                    let logs = updateLogs[v.source_id];
                    //find most recent one that has field in revert_patch;
                    //if can't find it, use the "create" UpdateLog
                    
                    for(let i=0; i < logs.length; i++) {
                        let ul = logs[i];
                        let rp = _.get(ul, 'revert_patch.'+field);
                        if(rp) {
                            v.date = ul.timestamp;
                            v.update_log = ul._id;
                            break;
                        }
                    }
                    if(!v.date) {
                        let ul = logs[logs.length-1];
                        if(ul.update_type === 'create') {
                            v.date = ul.timestamp;
                            v.update_log
                        }
                    }
                    
                    
                });
            });
            
            _.forEach(coalesceWith, (valueList, field)=>{
                var targetList = mergeData[field] = mergeData[field] || [];
                _.forEach(valueList, v=>{
                    let found = false;
                    targetList.forEach(t=>{
                        found = found || _.isEqual(t.value, v.value);
                    });
                    if(!found) {
                        targetList.push(v);
                    }
                });
            });
            
            _.forEach(mergeData, (valueList, field)=>{
                mergeData[field] = _.sortBy(valueList, 'date');
            });
            
        });
        
    };
    
    
    return function() {

        var keeper, dups;
        var className, dupIds, verIds, objDiffs;
        
        var origObjList, objMap;
        
        const mergeData = {};
        
        const redirectResults = [];
        var interceptors, interceptorResults, specConfig;
    
        return db.DupeSpec.findOne({_id:this.spec._id}).then(spec=>{
            
            specConfig = spec.config || {};
            
            if(spec.merge_interceptor) {
                interceptors = invoker.invokeInjected(spec.merge_interceptor, {}, spec);
            }
            else {
                interceptors = {};
            }
            
            var objPromise = this.getObjects();
            
            if(interceptors.preMerge instanceof Function) {
                objPromise = objPromise.then(objList=>{
                    return Q(interceptors.preMerge(this, objList, mergedVersion)).then(result=>{
                        if(result) {
                            interceptorResults = {preMerge:result};
                        }
                        return objList;
                    });
                });
            }
          
            return objPromise;
        })
        .then(objList=>{
            // logger.debug('%j\n***\n%j', this.objects, Object.keys(objMap));
            origObjList = _.clone(objList);
            objMap = _.keyBy(origObjList, '_id');
            
            //Pull off the first object as the keeper
            keeper = objList.shift();
            
            className = keeper._bo_meta_data.class_name;
            
            dups = objList.map(obj=>obj.toPlainObject());
            
            
            dupIds = _.map(dups, '_id');
            verIds = _.map(objList, '__ver');
            
            this.revert_data = {keeper:keeper.toPlainObject(), dups, className};
            this.status = 'in_process';
            
            return this.save().then(()=>MergeUtil.computeDiffs(origObjList));
        })
        .then(diffs=>{
            //Compute mergeData from diffs
            
            objDiffs = diffs;
            
            logger.debug('saving merge_data of %s %s', className, keeper._id);
            
            
            
            _.forEach(diffs, (compResult, field)=>{
                if(compResult.safe) {
                    keeper[field] = compResult.value;
                }
                else {
                    let md = mergeData[field] = [];
                    _.forEach(compResult.groups, g=>{
                        //if keeper is in this group, then it already has the value;
                        // we want to capture the values from other objects
                        if(g.indexOf(keeper._id) < 0) {
                            let obj = objMap[g[0]];
                            md.push({
                                value:obj[field],
                                source_id:obj._id
                            });
                        }
                    })
                }
            });
            
            return augmentMergeData(mergeData, keeper.merge_data);
        })
        .then(()=>{
            
            keeper.merge_data = mergeData;
            
            if(this.merge_vids) {
                var vid = new VersionId(verIds[0]);
                for(let i=1; i < verIds.length; i++) {
                    vid = VersionId.merge(vid, new VersionId(verIds[i]));
                }
                return keeper.save({useVersionId:vid.toString()});
            }
            else {
                return keeper.save();
            }
        })
        .then(()=>{
            //(3) re-direct references from other items to the keeper
            var promiseChain = Q(true);
            
            const redirectRefs = function(dup) {
                return MergeUtil.redirectIncomingReferences(className, dup._id, keeper._id).then(result=>{
                    redirectResults.push(result)
                });
            };
            
            
            dups.forEach(d=>{
                promiseChain = promiseChain.then(redirectRefs.bind(this, d));
            });
            
            return promiseChain;
        })
        .then(()=>{
            //Compile all redirect refs, stash into my revert_data
            this.revert_data.redirects = Array.prototype.concat.apply([], redirectResults);
            this.markModified('revert_data');
            
            this.status = 'merged';
            
            
            return Q.all([
                this.save(), 
                db[className].remove({_id:{$in:dupIds}})
            ]);
            
        })
        .then(()=>{
            
            if(interceptors.postMerge instanceof Function) {
                return Q(interceptors.postMerge(this, keeper)).then(result=>{
                    if(result) {
                        interceptorResults = interceptorResults || {};
                        interceptorResults.postMerge = result;
                    }
                });
            }
            
        })
        .then(()=>{
            this.status = 'merged';
            if(interceptorResults) {
                this.extended_info = this.extended_info || {};
                this.extended_info.interceptorResults = interceptorResults;
                this.markModified('extended_info');
            }
            return this.save();
        })
        .then(()=>{
            return {
                keeper:keeper._id,
                deleted:dupIds,
                redirectedRefs:_.get(this, 'revert_data.redirects'),
                interceptorResults,
                diffs:objDiffs,
                mergeData
            };
        })
        ;
        
        
    }
}