function(db, _, Q, MergeUtil, nodeRequire) {
    
    const invoker = nodeRequire('../util/invoker');
    
    const restoreObj = function(className, origObj, currObj, saveToList) {
        if(!currObj) {
            currObj = new db[className]();
        }
        let versionId = origObj.__ver;
        delete origObj.__ver;
        
        _.assign(currObj, origObj);
        
        saveToList && saveToList.push(currObj);
        
        return currObj.save({useVersionId:versionId});
    };
    

    
    return function() {
        if(this.status !== 'merged') {
            throw 'Must be status "merged" in order to revert';
        }
        
        const revertData = this.revert_data;
        if(!revertData.keeper ||  !revertData.dups ||  !revertData.className) {
            throw 'Missing revert data';
        }
        
        var interceptors, interceptorResults;
        var restoredObjects = [];
        
        return Q.all([
            db.DupeSpec.findOne({_id:this.spec._id}),
            db[revertData.className].findOne({_id:revertData.keeper._id})
        ])
        .spread((spec, keeper)=>{
            
        
            if(spec.merge_interceptor) {
                interceptors = invoker.invokeInjected(spec.merge_interceptor, {}, spec);
            }
            else {
                interceptors = {};
            }
            
            
            var promiseChain;
            
            if(interceptors.preRevert instanceof Function) {
                promiseChain = 
                    Q(interceptors.preRevert(this, keeper)).then(result=>{
                        if(result) {
                            interceptorResults = {preRevert:result};
                        }
                    });
            }
            else {
                promiseChain = Q(true);
            }
            
            promiseChain = promiseChain.then(restoreObj.bind(null, revertData.className, revertData.keeper, keeper, restoredObjects));
            
            _.forEach(revertData.dups, dup=>{
                promiseChain = promiseChain.then(restoreObj.bind(null, revertData.className, dup, null, restoredObjects));
            });
            
            if(revertData.redirects && revertData.redirects.length) {
                promiseChain = promiseChain.then(()=>{
                    return MergeUtil.restoreReferences(keeper._id, revertData.redirects);
                })
            }
            
            return promiseChain;
        })
        
        .then(()=>{
            this.status = 'reverted';
            return this.save();
        })
        .then(()=>{
            if(interceptors.postRevert instanceof Function) {
                return Q(interceptors.postRevert(this, restoredObjects)).then(result=>{
                    if(result) {
                        interceptorResults = interceptorResults || {};
                        interceptorResults.postRevert = result;
                    }
                });
            }
        })
        .then(()=>{
            if(interceptorResults) {
                this.extended_info = this.extended_info || {};
                this.extended_info.revertInterceptorResults = interceptorResults;
                this.markModified('extended_info');
                return this.save();
            }
        })
        .then(()=>{
            return {
                restoredObjects,
                interceptorResults
            };
        })
    }
}