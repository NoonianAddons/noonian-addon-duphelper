function(db, _, Q, nodeRequire) {
    
    const invoker = nodeRequire('../util/invoker');
    
    return function(forObj, falsePositiveTable) {
        var className = forObj._bo_meta_data.class_name;
        
        if(forObj.toPlainObject) {
            forObj = forObj.toPlainObject();
        }
        
        var compare;
        
        if(this.compare_fn) {
            try {
                compare = invoker.invokeInjected(this.compare_fn, {}, this);
            }
            catch(e) {
                console.error(e);
                compare = _.isEqual;
            }
        }
        else {
            compare = _.isEqual
        }
        
        
        const dupQuery = this.get_query ? this.get_query(forObj) : {};
        
        return db[className].find(dupQuery).lean().then(dupList => {
            const resultIds = [];
            
            _.forEach(dupList, d=> {
                if(d._id !== forObj._id && !(falsePositiveTable && falsePositiveTable.isMatched(d._id, forObj._id))) {
                    let result = compare(forObj, d);
                    result && resultIds.push({id:d._id, result});
                }
            });
            
            return resultIds;
        });
        
        
    }
}