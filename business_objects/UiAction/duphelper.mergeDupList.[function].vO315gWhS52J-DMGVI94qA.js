function(NoonWebService, DbuiAlert) {
    const dl = this.targetObj;
    
    NoonWebService.call('duphelper/mergeDupeList', {id:dl._id}).then(result=>{
        console.log('merge result', result);
        if(result.simple) {
            DbuiAlert.info('Simple merge completed.')
        }
        else {
            if(result && result.id) {
                $state.go('dbui.merge_dups', result);
            }
        }
    },
    err=>{
        console.error(err);
    });
}