function (db, MergeUtil, queryParams, Q, _) {
    
    if(!queryParams.id) {
        throw 'Missing required parameter(s)';
    }
    
    return db.DupeList.findOne({_id:queryParams.id}).then(function(dl) {
        if(!dl) {
            throw 'invalid DupeList id '+queryParams.id;
        }
        
        return dl.attemptAutoMerge();
    })
    .then(function(autoMergeResult) {
        
        //success -> update dupe list status; return message
        
        //fail -> redirect to merge_dups custompage
        
        return {message:'TODO'};
    });
}