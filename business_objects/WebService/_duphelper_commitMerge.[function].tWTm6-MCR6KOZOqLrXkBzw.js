function (db, queryParams, postBody, MergeUtil, _) {
    const dupeListId = queryParams.dupeList;
    const mergedObj = postBody && postBody.mergedObject;
    const deleteDuplicates = queryParams.deleteDuplicates;
    const keeperId = queryParams.keeper;
    
    if(!dupeListId || !mergedObj) {
        throw 'Missing required param(s)';
    }
    
    if(keeperId &&  mergedObj._id !== keeperId) {
        //If the custompage code behaves properly, we shouldn't ever see this
        console.error('Keeper ID doesnt match merged version id');
        mergedObj._id = keeperId;
    }
    
    return db.DupeList.findOne({_id:dupeListId}).then(dl=>{
        if(!dl) {
            throw 'invalid DupeList id:'+dupeListId;
        }
        
        if(deleteDuplicates !== 'true') {
            let className = dl.objects[0].ref_class;
            
            return db[className].findOne({_id:mergedObj._id}).then(keeper=>{
                delete mergedObj.__ver;
                _.assign(keeper, mergedObj);
            
                return keeper.save();
            }).then(keeper=>{
                return {message:'Successfully Saved '+keeper._disp};
            })
        }
        else {
            return dl.dedup(mergedObj, keeperId).then(result=>{
                console.log('Dedup result: ', result);
                let msg = `Deleted ${result.deleted.length} records and redirected ${result.redirectedRefs.length} references`;
                return {message:msg, result};
            });
        }
        
        
    })
    ;
    
    
}