function (db, MergeUtil, queryParams, Q, _) {
    const dupeListId = queryParams.dupelist;
    
    if(!dupeListId) {
        throw 'Missing required parameter(s)';
    }
    
    return db.DupeList.findOne({_id:dupeListId}).then(function(dl) {
        if(!dl || !dl.objects || dl.objects.length < 2) {
            throw 'invalid DupeList '+dupeListId;
        }
        
        if(dl.objects.length > 7) {
            throw 'Too many records; fix this with a script';
        }
        
        let ids = _.pluck(dl.objects, '_id');
        let className = dl.objects[0].ref_class;
        
        return Q.all([
            db[className].find({_id:{$in:ids}}).exec(),
            dl.identifyDiffs(true)
        ])
        .spread((objList, diffInfo)=>{
            let map = _.indexBy(objList, '_id');
            let sortedObjList = [];
            ids.forEach(id=>{sortedObjList.push(map[id])});
            return {
                className,
                dupeList:dl,
                objects:sortedObjList,
                diffInfo
            }
        });
        
        
    });
}