function (db, queryParams, postBody, DupeUtil, _, Q) {
    
    const id = queryParams.id;
    
    if(!id) {
        throw 'Missing required params'
    }
    
    
    return db.DupeList.findOne({_id:id}).then(dupeList=>{
        let ds = _.get(dupeList, 'spec._id');
        if(!ds) {
            throw 'invalid DupeList - missing spec';
        }
        
        return db.DupeSpec.findOne({_id:ds}).then(spec=>{
            const specCfg = spec.config || {};
            let simple = _.get(spec, 'config.simpleMerge');
            
            if(simple) {
                return dupeList.performSimpleMerge().then(mergeResult=>{
                    return {
                        simple:true,
                        result:mergeResult
                    };
                });
            }
            else {
                return {id:dupeList._id};
            }
            
        })
    });
    
}