function (db, queryParams, postBody, DupeUtil, _, Q) {
    const className = queryParams.className;
    const ids = queryParams.ids;
    
    if(!className || !ids || !ids.length) {
        throw 'Missing required params'
    }
    
    const refs = [];
    const query = {
        status:'outstanding',
        $and:[]
    };
    
    _.forEach(ids, id=>{
        query.$and.push({'objects._id':id});
        refs.push({_id:id, ref_class:className});
    });
    
    return Q.all([
        db.DupeList.findOne(query),
        db.DupeSpec.findOne({'classes.class_name':className})
    ])
    .spread((existing, spec)=>{
        if(existing && existing.objects.length === ids.length) {
            return existing;
        }
        
        if(!spec) {
            throw 'Missing DupeSpec for '+className;
        }
        
        const specCfg = spec.config || {};
        
        const newList = new db.DupeList({
            status:'outstanding',
            objects:refs,
            spec:{_id:spec._id},
            scan:'manually created'
        });
        
        return newList.save().then(dupeList=>{
            if(specCfg.simpleMerge) {
                return dupeList.performSimpleMerge().then(mergeResult=>{
                    return {
                        simple:true,
                        result:mergeResult
                    };
                });
            }
            else {
                return {id:dupeList._id};
            }
        });
    });
    
    
    
}