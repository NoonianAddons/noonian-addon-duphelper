function (db, queryParams) {
    const dupeListId = queryParams.id;
    
    if(!dupeListId) {
        throw 'Missing required param(s)';
    }
    
    return db.DupeList.findOne({_id:dupeListId}).then(dl=>{
        if(!dl) {
            throw 'invalid DupeList id:'+dupeListId;
        }
        
        return dl.revert();
    })
    .then(result=>{
        // console.log('Revert result: ', result);
        // let msg = `Deleted ${result.deleted.length} records and redirected ${result.redirectedRefs} references`;
        // return {message:msg, result};
        return {message:'Successfully reverted'};
    });
    
    
}