function (queryParams, DupeUtil, db, _, Q) {
    var defId = queryParams.id;
    var targetClass;
    
    var compareFn;
    var dupeCount = 0;
    
    if(DupeUtil.scanRunning) {
        return {message:'Scan is running: '+DupeUtil.scanRunning};
    }
    DupeUtil.scanRunning = require('moment')().format('LLL');
    
    return db.DupeSpec.findOne({_id:defId}).exec().then(function(ds) {
        if(!ds) {
            throw 'DupeSpec '+defId+' not found.';
        }
        
        if(!ds.classes || !ds.classes.length) {
            throw 'Invalid DupeSpec '+defId+' - missing field.';
        }
        
        var promiseChain = Q(true);
        const results = [];
        
        _.forEach(ds.classes, classRef=>{
            promiseChain = promiseChain.then(DupeUtil.scan.bind(DupeUtil, classRef._disp, ds.key, null, null)).then(result=>{
                results.push(result)
            });
        });
        
        return promiseChain.then(()=>{return results});
        
    }).then(function(resultList) {
        console.log(resultList);
        const scanLabels = _.pluck(resultList, 'scan');
        return {message: 'Scan: '+scanLabels.join(', ')};
        
    })
    .finally(()=>{
        DupeUtil.scanRunning = null;
    })
    ;
    
}